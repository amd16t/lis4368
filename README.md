> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 Advanced Web Applications

## Austin Davis

### LIS 4368 Requirements:


1. [A1 README.md](a1/README.md "My A1 README.md file")
    
    - Install jdk
    - Install tomcat
    - git commands
    - setup remote repo
    - screenshots of installations and server

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Create index.html
    - Create querybook.html
    - Complie HelloServlet.java
    - Compile AnotherHelloServlet.java
    - Compile QueryServlet.java 

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create database
    - Create a pet store, Pet and Customer table
    - Foward Engineer the database and export the SQL file

4. [P1 README.md](p1/README.md "My P1 README.md file") 

    - Add the following jQuery validation and regular expressions--as per the entity attribute requirements
    - Screenshots of Project
    - Canvas Links: Bitbucket repo

5. [A4 README.md](a4/README.md "My A4 README.md file")

    - Edit and compile CustomerServlet.java and Customer.java
    - Screenshots of Assignment
        - Screenshot of A4 Failed Validation
        - Screenshot of A4 Passed Validation
    - Canvas Links: Bitbucket repo

6. [A5 README.md](a5/README.md "My A5 README.md file") 

    - Edit and compile CustomerServlet.java, Customer.java, ConnectionPool.java, CustomerDB.java, and DBUtil.java
    - Screenshots of Project
    - Screenshot of A5 Vaild User Form Entry
    - Screenshot of A5 Passed Validation
    - Screenshot of Associated database entry
    - Canvas Links: Bitbucket repo 

7. [P2 README.md](p2/README.md "My P2 README.md file")

  - Edit and compile CustomerServlet.java, Customer.java, ConnectionPool.java, CustomerDb.java, and DButil.java
  - Screenshots of project 
  - Screenshot of P2 Valid User form entry
  - Screenshot of P2 Passed Validation
  - Screenshot of Associated database entry
  - Screenshot of display customers in database 
  - Canvas Links: Bitbucket repo 
