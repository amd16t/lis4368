> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 

## Austin Davis

### Assignment #2 Requirements:

Three Parts:

1. Assessment link screenshots
2. Screenshot of query result
3. Chapter questions

#### README.md file should include the following items:

* Screenshot of local host/hello
* Sceenshot of local host/index.html
* Screenshot of local host/sayhello
* Screenshot of local host/querybook.html
* Screenshot of local host/sayhi
* Screenshot of local host/querybook.html selected item
- http://localhost:9999/hello/querybook.html
* Screenshot of query result 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots: 
http://localhost:9999/hello

![Screenshot of http://localhost:9999/hello](img/localhost.png)
http://localhost:9999/hello/index.html

![Screenshot of http://localhost:9999/hello/index.html](img/hello.png)

http://localhost:9999/hello/HelloHome.html

![Screenshot of http://localhost:9999/hello/sayhello](img/helloworld.png)

http://localhost:9999/hello/querybook.html

![Screenshot of http://localhost:9999/hello/querybook.html](img/querybook.png)

http://localhost:9999/hello/querybook.html
![Screenshot of http://localhost:9999/hello/querybook.html](img/bookshop.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A3 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A3 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
