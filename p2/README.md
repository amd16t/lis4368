> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Austin Davis 

### Project 2 Requirements:


*Deliverables:*

*Four Parts:*

1. Edit and compile CustomerServlet.java, Customer.java, ConnectionPool.java, CustomerDB.java, and DBUtil.java
2. Screenshots of Project
    - Screenshot of P2 Vaild User Form Entry
    - Screenshot of P2 Passed Validation
    - Screenshot of Associated database entry
    - Screnshot of display customers in database
3. Canvas Links: Bitbucket repo
4. http://localhost:9999/lis4368/customerform.jsp?assign_num=a5


#### README.md file should include the following items:

- Screenshot of P2 Vaild User Form Entry
- Screenshot of P2 Passed Validation
- Screenshot of Associated database entry
- Screenshot of Display customers in database 


#### Assignment Screenshot:

*P2 Vaild User Form Entry*:
![A5 Vaild User Form Entry](img/p2_vufe.png "P2 Vaild User Form Entry")

*P2 Passed Validation*:
![A5 Passed Validation](img/p2_passed.png "P2 Passed Validation")

*Associated Database Entry*
![Associated Database Entry](img/p2_ADE.png "Associated Database Entry")

*Display Customers*
![Display Customers](img/p2_display.png "Display Customers")




