> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Austin Davis

### Assignment 1 Requirements:

Three posts

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Developement Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)
*  Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial);
* git commands w/ short descriptions
* Bitbucket repo links a) this assignment and b) the completed above 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - definition goes here...
2. git status
3. git add
4. git commit
5. git push
6. git pull
7. one additional git command

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/localhost.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hellojava.png)

*Screenshot of Android Studio - My First App*:

*Screenshot of Tomcat


<!-- [Android Studio Installation Screenshot](img/android.png) -->


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
