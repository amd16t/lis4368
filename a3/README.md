> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

#### LIS4368

#### Austin Davis  

### Assignment #3 Requirements:

*Deliverables:

1. Entity Relationship Diagram
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to repo must include README.md. using Markdowm syntax, and include links to all of the following files (From README.md)
   - docs folder: a3.mwb and a3.sql
   - img folder: a3.png (export a3.mwb file as a3.png)
   - README.md (MUST display a3.png ERD)
4. Canavs Links: Bitbucket repo
5. http://localhost:9999/lis4368/a3/index.jsp

#### Screenshot of A3 ERD that links to the image: 
![A3 ERD](img/a3.png)

#### A3 Docs:

[A3 MWB File](docs/a3.mwb)

[A3 SQl File](docs/a3.sql)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
