drop database if exists amd16t;
create database if not exists amd16t;
use amd16t;

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
    cus_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cus_first VARCHAR(15) NOT NULL,
    cus_last VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NOT NULL,
    cus_city VARCHAR(30) NOT NULL,
    cus_state CHAR(2) NOT NULL,
    cus_zip int(9) unsigned ZEROFILL NOT NULL,
    cus_phone bigint unsigned NOT NULL,
    cus_email VARCHAR(100) NOT NULL,
    cus_balance DECIMAL(6,2) NOT NULL,
    cus_total_sales DECIMAL(6,2) NOT NULL,
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (cus_id)
)

ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO customer
VALUES
(1,'Patti','Torres','855 West Fork Drive','Plantation','FL','081226749','20655598578','Pattitorres3@gmail.com',1234.00,1345.00,'customer notes1'),
(2,'Dianne','Rodrgiguez','2832 Simpson Square.','Oklahoma City','OK','73102','4566547908','DiannerodB@gmail.com',2234.00,2345.00,'customer notes2'),
(3,'Linda','Garcia','837 Matthews Street.','Rochelle','IL','61068','4560899785','Lindagarcia@gmail.com',3234.00,3345.00,'customer notes3'),
(4,'Matthew','Tellez','1196 Wyatt Street.','Boca Raton','FL','33432','3459879283','matthewtellez@gmail.com',4234.00,4345.00,'customer notes4'),
(5,'Amy','Nestor','1546 Foley Street','Ft Lauderdale','FL','33301','3450398576','amynestor@gmail.com',5234.00,5345.00,'customer notes5'),
(6,'Joshua','Collins','4299 Florence Street','Cardo Mills','TX','75135','2348768764','jcollins@gmail.com',6234.00,6345.00,'customer notes6'),
(7,'Mary','Williams','4779 Hoof Avenue','San Diego','CA','92103','2347658675','mwilliams@gmail.com',7234.00,7345.00,'customer notes7'),
(8,'Lisa','Larson','2689 University Street','Seattle','WA','98109','4350987564','lisalarson@gmail.com',8234.00,8345.00,'customer notes8'),
(9,'Donald','Parker','3519 Bicetown Road.','New York','NY','10013','8769549034','donaldparker@gmail.com',9234.00,9345.00,'customer notes9'),
(10,'Raymond','Smith','7399 N. Roosevelt St.','Clearwater','FL','33756','7689872365','RaymondS@gmail.com',345.00,536.00,'customer notes10');

SHOW WARNINGS;

DROP TABLE IF EXISTS petstore;
CREATE TABLE IF NOT EXISTS petstore
(
    pst_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pst_name VARCHAR(30) NOT NULL,
    pst_street VARCHAR(30) NOT NULL,
    pst_city VARCHAR(30) NOT NULL,
    pst_state CHAR(2) NOT NULL,
    pst_zip int(9) unsigned ZEROFILL NOT NULL,
    pst_phone bigint unsigned NOT NULL,
    pst_email VARCHAR(100) NOT NULL,
    pst_url VARCHAR(100) NOT NULL,
    pst_ytd_sales DECIMAL(10,2) NOT NULL,
    pst_notes VARCHAR(255) NULL,
    PRIMARY KEY (pst_id)
)

ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO petstore
(pst_id,pst_name,pst_street,pst_city,pst_state,pst_zip,pst_phone,pst_email,pst_url,pst_ytd_sales,pst_notes)
values
(1,'Sydney','1089 sw 66 st','Long Beach','CA','324509890','7777777777','syd@gmail.com','https://sydpets.com',4000.00,'loud'),
(2,'Riley','510 Daroco ave','Coral Gables','FL','673790675','9546113230','rileyl@hotmail.com','https://rileys.com',5000.00,'cute'),
(3,'Michael','6510 sw 10th st','Mexico City','MO','33134000','3059748140','michaeld@yahoo.com','https://michaels.com',6000.00,'none'),
(4,'Jordan','6969 nw 666 st','Hell','MI','768921345','5183924612','jordanl@aim.com','https://jordanstore.com',7000.00,'Turn around'),
(5,'Neil','1800 Pennsylvania ave','Washington','DC','760934864','4046147728','neil@me.com','https://neilspets.com',8000.00,'Winning'),
(6,'Jeremy','9653 Maple Drive','Atwater','CA','95301','9058537849','jeremyc@gmail.com','https://jeremystore.com',9000.00,'cheep'),
(7,'Glenn','4 South Parker St.','Paramount','CA','90723','2348798495','glenynyp@gmail.com','https://glennysuperstore.com',4200.00,'good'),
(8,'Julia','37 Arnold Dr.','Westminster','CA','92683','2903958675','jul@gmail.com','https://julias.com',4300.00,'bad'),
(9,'Madison','96 Inverness Street','San Francisco','CA','94110','2943445678','madison@gmail.com','https://madisons.com',4400.00,'rough'),
(10,'come get pets','9304 Westport St.','San Jose','CA','95127','2340984365','comeandgetthem@gmail.com','https://comegetpets.com',4500.00,'nope');

SHOW WARNINGS;
DROP TABLE IF EXISTS pet;
CREATE TABLE IF NOT EXISTS pet
(
    pet_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pst_id SMALLINT UNSIGNED NOT NULL,
    cus_id SMALLINT UNSIGNED NULL,
    pet_type VARCHAR(45) NOT NULL,
    pet_sex enum('m','f') NOT NULL,
    pet_cost DECIMAL (6,2) NOT NULL,
    pet_price DECIMAL (6,2) NOT NULL,
    pet_age TINYINT NOT NULL,
    pet_color VARCHAR(30) NOT NULL,
    pet_sale_data DATE NULL,
    pet_vaccine enum('y','n') NOT NULL,
    pet_neuter enum('y','n') NOT NULL,
    pet_notes VARCHAR(255) NULL,
    primary key(pet_id),

  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `amd16t`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `amd16t`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE SET NULL
      
)

ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO pet
(pet_id,pst_id,pet_type,pet_sex,pet_cost,pet_price,pet_age,pet_color,pet_vaccine,pet_neuter,pet_notes)
values
(1,1,'dog','f',300.00,320.00,2,'brown','y','y','cute'),
(2,2,'cat','m',700.00,710.00,6,'white','y','n','calm'),
(3,3,'dolphin','m',500.00,940.00,4,'blue','n','n','swim'),
(4,4,'lizard','m',200.00,520.00,2,'green','y','y','lick'),
(5,5,'parrot','f',600.00,230.00,10,'blue','n','y','flies'),
(6,6,'turtle','f',400.00,420.00,20,'green','y','y','aquatic'),
(7,7,'pig','m',720.00,730.00,16,'red','y','n','wild'),
(8,8,'monkey','m',870.00,970.00,5,'black','n','n','climb'),
(9,9,'lion','m',300.00,320.00,12,'light brown','y','y','jungle'),
(10,10,'goldfish','f',500.00,530.00,1,'orange','n','y','float');

SHOW WARNINGS;

select * from customer;
select * from pet;
select * from petstore;